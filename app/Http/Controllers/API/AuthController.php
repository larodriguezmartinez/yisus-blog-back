<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Laravel\Passport\HasApiTokens;


class AuthController extends Controller
{

   use HasApiTokens;

   public $successStatus = 200;

   public function register(Request $request) {


      $data = json_decode($request->getContent(), true);

      $validator = Validator::make($data, [
         'name' => 'required',
         'surname' => 'required',
         'legal' => 'required',
         'birthdate' => 'required',
         'email' => 'required|email',
         'password' => 'required',
      ]);


      if ($validator->fails()) {
         $success['message'] =  'error-data';
         return response()->json(['error'=>$validator->errors()], 401);
      }

      $data['password'] = bcrypt( $data['password']);

      if( User::create( $data) ){
         $success['message'] =  'user-create';
         return response()->json(['success'=>$success], $this->successStatus);
      }else{
         $success['message'] =  'error-create';
         return response()->json(['success'=>$success], 500);
      }

   }
      // $success['token'] =  $user->createToken('Yisus-Blog')->accessToken;





   public function login(Request $request)
   {

      $data = json_decode($request->getContent(), true);

      if(Auth::attempt(['email' => $data['email'], 'password' => $data['password'] ])){

         $user = Auth::user();

         $success['message'] = 'Login correcto';
         $success['user'] = $user;
         $success['token'] =  $user->createToken('Yisus-Blog')->accessToken;
         return response()->json(['success' => $success], $this->successStatus);
      } else {
         $success['message'] =  'error-data';
         return response()->json(['success' => $success], $this->successStatus);

      }
   }


   public function logout()
   {

      $user = Auth::user()->token();
      $user->revoke();

      return response()->json(['success'=>'Closed session'], $this->successStatus);
   }

   public function getUser()
   {
      $user = Auth::user();

      return response()->json(['success' => $user], $this->successStatus);
   }
}
