<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Post;
use App\User;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{

    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $posts = Post::orderBy('created_at', 'desc')->with(['images', 'user'])->get();

        if($posts){
            $success['message'] = 'Post list';
            $success['posts'] = $posts;
            return response()->json(['success'=>$success], $this->successStatus);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = json_decode($request->getContent(), true);

        $input = $request->all();

        $user = \Auth::user();
        $data['user_id'] = $user->id;

        $validator = Validator::make($data, [
            'title' => 'required',
            'subtitle' => 'required',
            'content' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $post = Post::create($data);
        $imageData['post_id'] = $post->id;

        for ($i=0; $i < 4 ; $i++) {
            $number = rand(1031,1083);
            $imageData['path'] = 'https://picsum.photos/id/'.$number.'/250/';
            Image::create($imageData);
        }


        if($post) {
            $success['message'] = 'Post create successfully';
            $success['id'] = $post->id;
        }else{
            $success['message'] = 'Try later.';
            $this->successStatus = 503;
        }


        return response()->json(['success'=>$success], $this->successStatus);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $post = Post::with(['user','images'])->find($id);
        if ( $post ) {
            $success['post'] = $post;
            $success['message'] = 'Post find';
        }else{
            $success['message'] = 'Post not found.';
            $this->successStatus  = 404;
        }

        return response()->json(['success'=>$success], $this->successStatus);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , $id)
    {
        $reqdata = $request->all();

        $validator = Validator::make($reqdata, [
            'title' => 'required',
            'subtitle' => 'required',
            'content' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);

        }

        $post = Post::find($id);
        $post->title = $reqdata['title'];
        $post->subtitle = $reqdata['subtitle'];
        $post->content = $reqdata['content'];

        if($post->save()){
            $success['message'] = 'Post updated successfully.';
        }else{
            $success['message'] = 'Try later.';
            $this->successStatus = 503;
        }


        return response()->json(['success'=>$success], $this->successStatus);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if(Post::where('id', $id)->delete()){
            $success['message'] = 'Post deleted successfully.';
        }else{
            $success['message'] = 'Try later.';
            $this->successStatus = 503;
        }

        return response()->json(['success'=>$success], $this->successStatus);

    }
}
