<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Alberto',
            'surname' => 'Rodriguez',
            'email' => 'larodriguezmartinez@gmail.com',
            'birthdate' => '1983-01-12',
            'legal' => true ,
            'profile_id' => 1,
            'password' => bcrypt('123456')
        ]);
    }
}
