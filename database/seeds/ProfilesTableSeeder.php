<?php

use App\Profile;
use Illuminate\Database\Seeder;


class ProfilesTableSeeder extends Seeder
{
   /**
   * Run the database seeds.
   *
   * @return void
   */
   public function run()
   {
        DB::table('profiles')->insert([
            'id' => 1,
            'profile_name' => 'Admin',
        ]);

        DB::table('profiles')->insert([
            'id' => 2,
            'profile_name' => 'User',
        ]);


   }
}
